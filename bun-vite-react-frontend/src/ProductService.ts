const PAGE_SIZE = 8;
const BASE_URL = "http://localhost:5131"

export async function getProducts(page){
  return fetch(`${BASE_URL}/ProductList/page/${page || 1}`)
}

export async function getProductsDetails(id){
  return fetch(`${BASE_URL}/Product/${id}`)
}
