import './ProductDetails.css'
export default function ProductDetails({product, back}){

  return (
    <main>
      <button onClick={back}>Back</button>
      <div className="product-details">
        <div className="image-container">
          <img src={product.image} alt="Product image for {product.title}" />
        </div>
        <div>
          <h1>{product.title}</h1>
          <p>{product.description}</p>
        </div>
        <div>
          <p className="price">{product.price} KR</p>
          <span className="product-rating">{product.rating.rate} ({product.rating.count})</span>
        </div>
      </div>
    </main>
  )
}
