import './ProductList.css';

export default function ProductList({ products, showDetails}) {
  const rows = [];

  products.forEach((product) => {
    rows.push(
      <ProductListItem key={product.id} product={product} showDetails={showDetails} />
    );
  })

  return (
    <main>
      <h1>Products</h1>
      <ul id="product-list">{rows}</ul>
    </main>
  )
}

function ProductListItem({ product, showDetails }) {

  return ( 
    <li onClick={e => showDetails(product.id)}>
      <div className="image-container">
        <img src={product.image} alt="Product image" />
      </div>
      <div className="middle-column">
        { product.category }<br />
        <h2>{ product.title }</h2>
      </div>
      <div className="price-column">{ product.price } KR</div>
    </li>
  )
}

