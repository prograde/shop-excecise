import { useState, useEffect } from 'react'
import './App.css'
import ProductDetails from './ProductDetails.tsx'
import ProductList from './ProductList.tsx'
import { getProducts, getProductsDetails } from './ProductService.ts'
import Paginator from './Paginator.tsx'

function App() {
  const [productToShow, setProductToShow] = useState(null);
  const [productId, setProductId] = useState(null);
  const [productList, setProductList] = useState([]);
  const [page, setPage] = useState(1);

  useEffect(() => {
    async function fetchData() {
      const res = await getProducts(page)
      const data = await res.json();
      if(Array.isArray(data))
        setProductList(data);
      else setPage(page-1)
    }

    if (page < 1) setPage(1)
    fetchData();
  }, [page]);

  useEffect(() => {
    async function fetchProductDetails(){
      const res = await getProductsDetails(productId)
      const data = await res.json()
      setProductToShow(data)
    }

    !productId || fetchProductDetails(productId)
  }, [productId])

  function showDetails(productId){
    setProductId(productId)
  }

  function back(e) {
    setProductId(null)
    setProductToShow(null)
  }

  function prevPage(){
    setPage(page - 1)
  }

  function nextPage(){
    setPage(page + 1)
  }

  if (productToShow) return (<ProductDetails product={productToShow} back={back} />)
  else return (
    <>
      <ProductList products={productList} showDetails={showDetails} />
      <Paginator prev={prevPage} next={nextPage} pageNumber={page} />
    </>
  )
}

export default App

