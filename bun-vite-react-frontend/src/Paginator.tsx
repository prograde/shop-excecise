export default function Paginator({pageNumber, prev, next}) {
  return (
    <div className="paginator">
      <button onClick={prev}>prev</button>
      Page {pageNumber}
      <button onClick={next}>next</button>
    </div>
  )
}
