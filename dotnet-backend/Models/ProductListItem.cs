namespace dotnet_backend;

public class ProductListItem
{
    public int Id { get; set; }
    public string Title { get; set; }
    public string Category { get; set; }
    public float Price { get; set; }
    public string Image { get; set; }
}