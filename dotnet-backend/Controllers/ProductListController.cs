using Microsoft.AspNetCore.Mvc;
using System.Collections;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.HttpResults;

namespace dotnet_backend.Controllers;

[ApiController]
[Route("[controller]")]
public class ProductListController: ControllerBase
{
    private IWebHostEnvironment Environment;

    public ProductListController(IWebHostEnvironment _environment)
    {
        Environment = _environment;
    }

    [Route("page/{page}")]
    [HttpGet]
    public IActionResult Get(int page)
    {
        const int PAGE_SIZE  = 8;

        // TODO: move to data layer
        string filename = this.Environment.ContentRootPath+"/Data/mockproducts.json";
        var jsonProductList = System.IO.File.ReadAllText(filename);
        var productList = JsonConvert.DeserializeObject<List<ProductListItem>>(jsonProductList);

        var startIndex = Math.Max((page-1)*PAGE_SIZE, 0);
        if (startIndex > productList.Count())
            return NotFound();

        var count = PAGE_SIZE - Math.Max(0, (startIndex + PAGE_SIZE - productList.Count()));
        productList = productList.GetRange(startIndex, count);

        return Ok(productList.ToArray());
    }
}
