using Microsoft.AspNetCore.Mvc;
using System.Collections;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace dotnet_backend.Controllers;

[ApiController]
[Route("[controller]")]
public class ProductController: ControllerBase
{
    private IWebHostEnvironment Environment;

    public ProductController(IWebHostEnvironment _environment)
    {
        Environment = _environment;
    }

    [Route("{productId}")]
    [HttpGet]
    public IActionResult Get(int productId)
    {
        // TODO: move to data layer
        string filename = this.Environment.ContentRootPath+"/Data/mockproducts.json";
        var jsonProducts = System.IO.File.ReadAllText(filename);
        var products = JsonConvert.DeserializeObject<List<Product>>(jsonProducts);
        var product =  products.Find( x => x.Id == productId);
        if(product == null) {
            return NotFound();
        }
        return Ok(product);
    }
}