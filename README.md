# React + TypeScript + Vite

## Considerations

### The setup

- *Bun*: shining new, blazing fast JS bundler, package-manager, runtime and toolkit. Must test!
- *Vite*: Although Bun is said to be an all-in-one-toolkit, we still [need for a bundler for React](https://dev.to/this-is-learning/why-use-vite-when-bun-is-also-a-bundler-vite-vs-bun-2723), and Vite is popular nowdays
- *TypeScript* is not necessary in my opinion, but most devs want it so that's why I decided to use it
- *SWC* for Speedy Web Compilation, as an alternative to good old Babel.

So, getting started like this:

```
bun create vite
```

lets me select first the React template, then to use TypeScript rendered with SWC. ESLint is also set up.

### Breaking down in components

First of all, we need a ProductsList component, and a ProductListItem component. I chose to display the items with flex. I could have chosen a table instead. The images are normal img tags. In an optimized version, I would preload and cache images.

Secondly, we need a ProductDetails component. I use flex for that aswell.

### Paging

In a Single Page Application there are no real pages, but we use the state to simulate a ProductList page and a ProductDetails page. This can be done in different ways, and a better way would be to introduce a router, e.g. react-router, to also give support for route history. I made a simpler solution for this example app. 

### Creating a service layer in frontend

The service is called upon via `useEffect`, and the ProductList is in state, to be able to update it asyncronly.

### Backend 

The backend has two controllers, ProductList and ProductDetails. 
